//
//  GameObject.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "Constants.h"
#import "CommonProtocols.h"
#import <SpriteKit/SpriteKit.h>

@interface GameObject : SKSpriteNode {
    GameObjectType _gameObjectType;
}

@property (nonatomic) GameObjectType gameObjectType;

@property (nonatomic) SKTextureAtlas *projectilesAtlas;
@property (nonatomic) SKTextureAtlas *backgroundAtlas;

@property (nonatomic) SKTextureAtlas *explosionAtlas;
@property (nonatomic) SKAction *explosionAnimation;

-(void)changeState:(CharacterStates)newState;
-(SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName;



@end
