//
//  HeroShip.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/24/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "HeroShip.h"


@implementation HeroShip

-(instancetype)initWithhSize:(CGSize)size{
    if (self = [super init]){
        self.heroShipAtlas = [self textureAtlasNamed:@"heroship"];
        self.screenSize = size;
        projectileSpeed = 2;
        self.characterHealth = 1;
        self.currentHealth = self.characterHealth;
        _shipHit = NO;
    }
    return self;
}


-(void)spawnShip{
    _heroShip = [[SKSpriteNode alloc] initWithTexture:[self.heroShipAtlas textureNamed:@"blueship6"]];
    _heroShip.name = @"heroShip";
    [self addChild:_heroShip];
    self.physicsBody = [SKPhysicsBody bodyWithTexture:[self.heroShipAtlas textureNamed:@"blueship6"]  size:_heroShip.size];
    self.physicsBody.dynamic = YES;
    self.physicsBody.affectedByGravity = YES;
    self.physicsBody.allowsRotation = NO;
    self.physicsBody.categoryBitMask = heroShipBitMask;
    self.physicsBody.contactTestBitMask = pillBitMask | enemyFireBitMask;
    self.physicsBody.collisionBitMask =  worldBitMask;
    self.physicsBody.usesPreciseCollisionDetection = YES;
    [self healthBar:_heroShip health:self.characterHealth currentHealth:self.currentHealth];
}

-(void)fireWeapon{
    if (self.weaponReady){
        FireWeapon *fire = [FireWeapon new];
        [fire changeState:kStateSpawning];
        fire.position = CGPointMake(_heroShip.frame.size.width / 2 , _heroShip.position.y );
        fire.name = @"blueFire";
        fire.physicsBody.contactTestBitMask = enemyShipBitMask | largeMeteorBitMask | mediumMeteorBitMask | smallMeteorBitMask;
        [self addChild:fire];
        SKAction *moveFire = [SKAction moveToX:self.screenSize.width + _heroShip.size.width duration:projectileSpeed];
        SKAction *remove = [SKAction removeFromParent];
        [fire runAction:[SKAction sequence:@[moveFire, remove]]];
        /*self.weaponReady = NO;
         [self runAction:[SKAction waitForDuration:projectileSpeed / 2] completion:^{
         self.weaponReady = YES;
         }];*/
    }
}

-(void)shipDead{
    if (self.currentHealth <= 0){
        self.isAlive = NO;
        [_heroShip runAction:self.explosionAnimation completion:^{
            [_heroShip removeFromParent];
            
        }];
    } else {
        if (self.currentHealth > self.characterHealth){
            self.currentHealth = self.characterHealth;
        }
        [self healthBar:_heroShip health:self.characterHealth currentHealth:self.currentHealth];
    }
}

-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            [self spawnShip];
            break;
        case kStateFiring:
            [self fireWeapon];
            break;
        case kStateDead:
            [self shipDead];
        default:
            break;
    }
}


@end
