//
//  WeaponsFire.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/24/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameCharacter.h"

@interface FireWeapon : GameCharacter

@property (nonatomic) float weaponDamage;


@end
