//
//  GameManager.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>


extern NSString *const PresentAuthenticationViewController;

@interface GameManager : NSObject  {
    BOOL _enabledGameCenter;
}

@property (nonatomic) BOOL enabledGameCenter;
@property (nonatomic, readonly) UIViewController *authenticationViewController;
@property (nonatomic, readonly) NSError *lastError;
@property (nonatomic) int highScore;

-(void)authenticateLocalPlayer;
+(instancetype) sharedData;
-(void)setAuthenticationViewController:(UIViewController *)authenticationViewController;
-(void)setLastError:(NSError *)error;
@end
