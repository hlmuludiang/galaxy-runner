//
//  GameManager.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameManager.h"

NSString *const PresentAuthenticationViewController = @"present_authentication_view_controller";

@implementation GameManager

//static variable - this stores our singleton instance
static GameManager *sharedData = nil;

+(instancetype) sharedData
{
    //If our singleton instance has not been created (first time it is being accessed)
    if(sharedData == nil)
    {
        //create our singleton instance
        sharedData = [[GameManager alloc] init];
        
        //collections (Sets, Dictionaries, Arrays) must be initialized
        //Note: our class does not contain properties, only the instance does
        //self.arrayOfDataToBeStored is invalid
    }
    //if the singleton instance is already created, return it
    return sharedData;
}


- (BOOL)isGameCenterAvailable {
    NSLog(@"Checking for game center");
    // check for presence of GKLocalPlayer API
    Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    
    // check if the device is running iOS 4.1 or later
    NSString *reqSysVer = @"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer
                                           options:NSNumericSearch] != NSOrderedAscending);
    
    return (gcClass && osVersionSupported);
}

-(id)init{
    if (self = [super init]){
        _highScore = 0;
        NSLog(@"%i", _enabledGameCenter);
        _enabledGameCenter =  [self isGameCenterAvailable];
        NSLog(@"%i", _enabledGameCenter);
    }
    NSLog(@"Singleton created");
    return self;
}

-(void)authenticateLocalPlayer{
    NSLog(@"Authenitcating local user");
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        [self setLastError:error];
        if (viewController != nil) {
            [self setAuthenticationViewController:viewController];
        } else if ([GKLocalPlayer localPlayer].isAuthenticated){
            _enabledGameCenter = YES;
            NSLog(@"All good");
        } else {
            _enabledGameCenter = NO;
            NSLog(@"No good :-(");
        }
    };
}

-(void)setAuthenticationViewController:(UIViewController *)authenticationViewController{
    NSLog(@"Getting view controller");
    if (authenticationViewController != nil) {
        _authenticationViewController = authenticationViewController;
        [[NSNotificationCenter defaultCenter] postNotificationName:PresentAuthenticationViewController object:self];
    }
}

-(void)setLastError:(NSError *)error{
    _lastError = [error copy];
    if (_lastError){
        NSLog(@"GameKitHelper Error: %@", [[_lastError userInfo] description]);
    }
}

@end
