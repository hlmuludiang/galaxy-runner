//
//  Pills.h
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/8/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "SpaceObjects.h"

@interface Pills : SpaceObjects

@property (nonatomic) SKTextureAtlas *pillTextureAtlas;
@property (nonatomic) NSMutableArray *pillsRotatingArray;
@property (nonatomic) NSMutableArray *pillTexturesArray;

@property (nonatomic) SKSpriteNode *pillNode;

@property (nonatomic) float pillStrength;

@end
