//
//  HUD.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/26/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameCharacter.h"

@interface HUD : GameCharacter

@property (nonatomic) SKLabelNode *HUDscoreLabel;
@property (nonatomic) SKLabelNode *weaponsStatusLabel;
-(instancetype)initWithSize:(CGSize)size;
-(void)updateScore;

@end
