//
//  WeaponsFire.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/24/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "FireWeapon.h"

@implementation FireWeapon

-(instancetype)init{
    if (self = [super init]){
        _weaponDamage = .5f;
    }
    return self;
}


-(void)blueFire{
    SKSpriteNode *fire = [[SKSpriteNode alloc] initWithTexture:[self.projectilesAtlas textureNamed:@"attack_blue"]];
    fire.name = @"blueFire";
    //fire.size = CGSizeMake(fire.size.width * .25, fire.size.height * .25);
    [self addChild:fire];
    
    self.physicsBody = [SKPhysicsBody bodyWithTexture:[self.projectilesAtlas textureNamed:@"attack_blue"] size:fire.size];
    self.physicsBody.dynamic = NO;
    self.physicsBody.affectedByGravity = NO;
    self.physicsBody.allowsRotation = NO;
    self.physicsBody.categoryBitMask = blueFireBitMask;
    self.physicsBody.contactTestBitMask = 0;
    self.physicsBody.collisionBitMask =  0;
    self.physicsBody.usesPreciseCollisionDetection = YES;
}

-(void)weaponUsed{
    self.isAlive = NO;
    [[self childNodeWithName:@"blueFire"] removeFromParent];
}


-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            [self blueFire];
            break;
        case kStateDead:
            [self weaponUsed];
            break;
        default:
            break;
    }
}
@end
