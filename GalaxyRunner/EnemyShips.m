//
//  EnemyShips.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 10/2/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "EnemyShips.h"

@implementation EnemyShips

-(instancetype)init{
    if (self = [super init]){
        _gameObjectType = kCharacterTypeEnemy;
        self.physicsBody.contactTestBitMask = heroShipBitMask;
    }
    return self;
}

@end
