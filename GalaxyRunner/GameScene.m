//
//  GameScene.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene

-(instancetype)initWithSize:(CGSize)size{
    if (self = [super initWithSize:size]){
        _gameRunning = NO;
        _restartGame = NO;
        self.backgroundColor = [SKColor clearColor];
        self.physicsWorld.gravity = CGVectorMake(-.5f, 0.0f);
        self.physicsWorld.contactDelegate = self;
        self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:CGRectMake(10, 0, self.frame.size.width, self.frame.size.height)];
        self.physicsBody.collisionBitMask = heroShipBitMask;
        self.physicsBody.categoryBitMask = worldBitMask;
        _level = 1;
        
        _backGround = [[FMMParallaxNode alloc] initWithBackground:@"starbackground" size:self.frame.size pointsPerSecondSpeed:15];
        [self addChild:_backGround];
        
        
        _planets = [[Planets alloc] initWithSize:size];
        [_planets changeState:kStateSpawning];
        _planets.position = CGPointMake(self.frame.size.width * .75, self.frame.size.height * .66);
        [self addChild:_planets];
        /*NSString *emitterName = [[NSBundle mainBundle] pathForResource:@"StarField" ofType:@"sks"];
         SKEmitterNode *starField = [NSKeyedUnarchiver unarchiveObjectWithFile:emitterName];
         [self addChild:starField];*/
        
        _myHUD = [[HUD alloc] initWithSize:size];
        [self addChild:_myHUD ];
        _score = 0;
        
        [self levelDisplay];
        
        
        [self generateMeteors];
        //[self generatePills];
        
        _pauseMenu = [[JSGameMenu alloc] initWithSize:size withColorTheme:[UIColor whiteColor] withReplayHandler:^{
            [self restartGame];
        } withExitHandler:^{
            [self exitGame];
        }];
        _pauseMenu.name = @"pauseMenu";
        [self addChild:_pauseMenu];
    }
    return self;
}

-(void)restartGame{
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Restart Game?" message:@"Are you sure you want to restart the game?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    alertView.tag = 2;
    [alertView show];

    NSLog(@"Restart game");
    //_myHUD.HUDscoreLabel.text = @"Score: 0000";

}

-(void)generatePills{
    _pills = [Pills new];
    [_pills changeState:kStateSpawning];
    _pills.name = @"pillNodeRoot";
    [self addChild:_pills];
    [_pills shipPath:_pills frameSize:self.frame.size shipSpeed:10];
}

-(void)addHeroShip{
    _heroShip = [[HeroShip alloc] initWithhSize:self.frame.size];
    [_heroShip changeState:kStateSpawning];
    _heroShip.name = @"heroShipRoot";
    _heroShip.position = CGPointMake(self.frame.size.width * .33, self.frame.size.height * .5);
    [self addChild:_heroShip];
    
}

-(void)levelDisplay{
    SKLabelNode *levelText = [[SKLabelNode alloc] initWithFontNamed:@"DystopianFuture"];
    levelText.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    levelText.text = [NSString stringWithFormat:@"Level %i", _level];
    levelText.fontSize = 80;
    [levelText setScale:0];
    
    [self addChild:levelText];
    SKAction *hide = [SKAction fadeAlphaTo:0 duration:4];
    SKAction *grow = [SKAction scaleTo:1 duration:3];
    
    [levelText runAction:[SKAction group:@[hide,grow]] completion:^{
        [self addHeroShip];
        _gameRunning = YES;
        //[self addAndMoveShip];
    }];
}

-(void)addAndMoveShip{
    
    for (int x = 1; x < 7; x++){
        
        _reconShip = [[ScoutShip alloc] initWithSize:self.frame.size];
        [_reconShip changeState:kStateSpawning];
        _reconShip.name = @"reconShipRoot";
        [self addChild:_reconShip];
        [_reconShip shipPath:_reconShip frameSize:self.frame.size shipSpeed:_reconShip.shipSpeed];
    }
    
    for (int x = 1; x < 4; x++) {
        _patrolShip = [[PatrolShip alloc] initWithSize:self.frame.size];
        [_patrolShip changeState:kStateSpawning];
        _patrolShip.name = @"patrolShipRoot";
        //[self addChild:_patrolShip];
        [_patrolShip shipPath:_patrolShip frameSize:self.frame.size shipSpeed:_patrolShip.shipSpeed];
    }
        _cruiser = [[CrusierShip alloc] initWithSize:self.frame.size];
        [_cruiser changeState:kStateSpawning];
        _cruiser.name = @"cruiserShipRoot";
        //[self addChild:_cruiser];
        [_cruiser shipPath:_cruiser frameSize:self.frame.size shipSpeed:_cruiser.shipSpeed];
        
        _destroyerShip = [[Destroyer alloc] initWithSize:self.frame.size];
        [_destroyerShip changeState:kStateSpawning];
        _destroyerShip.name = @"destroyerShipRoot";
        //[self addChild:_destroyerShip];
        [_destroyerShip shipPath:_destroyerShip frameSize:self.frame.size shipSpeed:_destroyerShip.shipSpeed];
    
        _battleCruiserShip = [[BattleCruiser alloc] initWithSize:self.frame.size];
        [_battleCruiserShip changeState:kStateSpawning];
        _battleCruiserShip.name = @"battleCruiserShipRoot";
        //[self addChild:_battleCruiserShip];
        [_battleCruiserShip shipPath:_battleCruiserShip frameSize:self.frame.size shipSpeed:_battleCruiserShip.shipSpeed];
}

-(void)generateMeteors{
    SKAction *moveMeteor = [SKAction moveToX:-self.frame.size.width duration:7];
    SKAction *removeMeteor = [SKAction removeFromParent];
    
    int x = arc4random_uniform(3);
    
    if (x == 0){
        LargeMeteors *largeMeteor = [LargeMeteors new];
        largeMeteor.position = CGPointMake([largeMeteor getRandomNumberBetween:self.frame.size.width to:self.frame.size.width * 1.25], [largeMeteor getRandomNumberBetween:0 to:self.frame.size.height]);
        [self addChild:largeMeteor];
        largeMeteor.name = @"meteorNodeRoot";
        [largeMeteor changeState:kStateSpawning];
        
        [largeMeteor runAction:[SKAction sequence:@[moveMeteor, removeMeteor]]];
    } else if (x == 1){
        MediumMeteors *mediumMeteor = [MediumMeteors new];
        mediumMeteor.position = CGPointMake([mediumMeteor getRandomNumberBetween:self.frame.size.width to:self.frame.size.width * 1.25], [mediumMeteor getRandomNumberBetween:0 to:self.frame.size.height]);
        [self addChild:mediumMeteor];
        mediumMeteor.name = @"meteorNodeRoot";
        [mediumMeteor changeState:kStateSpawning];
        
        [mediumMeteor runAction:[SKAction sequence:@[moveMeteor, removeMeteor]]];
    } else {
        SmallMeteors *smallMeteor = [SmallMeteors new];
        smallMeteor.position = CGPointMake([smallMeteor getRandomNumberBetween:self.frame.size.width to:self.frame.size.width * 1.25], [smallMeteor getRandomNumberBetween:0 to:self.frame.size.height]);
        [self addChild:smallMeteor];
        smallMeteor.name = @"meteorNodeRoot";
        [smallMeteor changeState:kStateSpawning];
        
        [smallMeteor runAction:[SKAction sequence:@[moveMeteor, removeMeteor]]];
    }
}

-(void)gameOver{
    SKLabelNode *levelText = [[SKLabelNode alloc] initWithFontNamed:@"DystopianFuture"];
    levelText.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    levelText.text = [NSString stringWithFormat:@"Game Over"];
    levelText.fontSize = 70;
    [levelText setScale:0];
    
    [self addChild:levelText];
    SKAction *hide = [SKAction fadeAlphaTo:1 duration:4];
    SKAction *grow = [SKAction scaleTo:1 duration:3];
    
    [levelText runAction:[SKAction group:@[hide,grow]] completion:^{
    }];
}

-(void)didMoveToView:(SKView *)view {
}

-(void)didBeginContact:(SKPhysicsContact *)contact{
    FireWeapon *fire;
    ScoutShip *ship;
    HeroShip *myShip;
    Pills *pill;
    
    if (((contact.bodyA.categoryBitMask == blueFireBitMask) && (contact.bodyB.categoryBitMask == enemyShipBitMask))
        || ((contact.bodyA.categoryBitMask == enemyShipBitMask) && (contact.bodyB.categoryBitMask == blueFireBitMask))) {
        
        if (contact.bodyA.categoryBitMask == blueFireBitMask) {
            fire = (FireWeapon *)contact.bodyA.node;
            ship = (ScoutShip *)contact.bodyB.node;
        } else {
            fire = (FireWeapon *)contact.bodyB.node;
            ship = (ScoutShip *)contact.bodyA.node;
        }
        
        if (fire.isAlive && ship.isAlive) {
            ship.currentHealth -= fire.weaponDamage;
            [fire changeState:kStateDead];
            [ship changeState:kStateDead];
            
            if (!ship.isAlive) {
                _myHUD.HUDscoreLabel.text = [NSString stringWithFormat:@"Score: %04u", _score = _score + 1 ];
                [self runAction:[SKAction waitForDuration:.7] completion:^{
                    [ship removeFromParent];
                }];
            }
            [fire removeFromParent];
            
            
        }
    } else if (((contact.bodyA.categoryBitMask == heroShipBitMask) && (contact.bodyB.categoryBitMask == pillBitMask)) ||
               ((contact.bodyA.categoryBitMask == pillBitMask) && (contact.bodyB.categoryBitMask == heroShipBitMask ))) {
        if (contact.bodyA.categoryBitMask == heroShipBitMask) {
            myShip = (HeroShip *)contact.bodyA.node;
            pill = (Pills *)contact.bodyB.node;
        } else {
            myShip = (HeroShip *)contact.bodyB.node;
            pill = (Pills *)contact.bodyA.node;
            
        }
        if (pill.isAlive){
            myShip.currentHealth += pill.pillStrength;
            [myShip changeState:kStateDead];
            [pill changeState:kStateDead];
            [pill removeFromParent];
        }
        
    } else if (((contact.bodyA.categoryBitMask == heroShipBitMask) && (contact.bodyB.categoryBitMask == enemyFireBitMask)) ||
               ((contact.bodyA.categoryBitMask == enemyFireBitMask) && (contact.bodyB.categoryBitMask == heroShipBitMask ))) {
        if (contact.bodyA.categoryBitMask == heroShipBitMask) {
            myShip = (HeroShip *)contact.bodyA.node;
            fire = (FireWeapon *)contact.bodyB.node;
        } else {
            myShip = (HeroShip *)contact.bodyB.node;
            fire = (FireWeapon *)contact.bodyA.node;
            
        }
        if (fire.isAlive && myShip.isAlive){
            myShip.currentHealth -= fire.weaponDamage;
            myShip.shipHit = YES;
            [myShip changeState:kStateDead];
            [fire changeState:kStateDead];
            [fire removeFromParent];
            if (!myShip.isAlive) {
                [self runAction:[SKAction waitForDuration:.7] completion:^{
                    [myShip removeFromParent];
                    [self gameOver];
                }];
            }
        }
    }
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    for (UITouch *touch in touches){
        CGPoint touchPosition = [touch locationInNode:self];
        if ([[self nodeAtPoint:touchPosition].name isEqualToString:@"pause"]){
            [_pauseMenu handleTouch:touches withEvent:event];
        } else if ([[self nodeAtPoint:touchPosition].name isEqualToString:@"play"]){
            [_pauseMenu handleTouch:touches withEvent:event];
        } else if ([[self nodeAtPoint:touchPosition].name isEqualToString:@"replay"]){
            [_pauseMenu handleTouch:touches withEvent:event];
        } else if ([[self nodeAtPoint:touchPosition].name isEqualToString:@"exit"]) {
            [_pauseMenu handleTouch:touches withEvent:event];
        } else if (!_pauseMenu.isPaused){
            [_heroShip changeState:kStateFiring];
        }
    }
}

-(void)exitGame{
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Quit Game?" message:@"Are you sure you want to quit the game?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    alertView.tag = 1;
        [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1){
        if (buttonIndex == 0){
            return;
        } else {
            SKScene *scene = [MenuScene sceneWithSize:self.frame.size];
            scene.scaleMode = SKSceneScaleModeAspectFill;
            // Present the scene.
            [self.view presentScene:scene transition:[SKTransition doorwayWithDuration:2]];
        }
    } else if (alertView.tag == 2){
        if (buttonIndex == 0){
            return;
        } else {
            SKScene *scene = [GameScene sceneWithSize:self.frame.size];
            scene.scaleMode = SKSceneScaleModeAspectFill;
            
            // Present the scene.
            [self.view presentScene:scene transition:[SKTransition doorsOpenVerticalWithDuration:2]];
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    [_backGround update:currentTime];
    
    
    if (_heroShip.isAlive && _gameRunning){
        NSString *emitterName = [[NSBundle mainBundle] pathForResource:@"ShipExhaust" ofType:@"sks"];
        SKEmitterNode *shipExhaust = [NSKeyedUnarchiver unarchiveObjectWithFile:emitterName];
        
        [self enumerateChildNodesWithName:@"reconShipRoot" usingBlock:^(SKNode *node, BOOL *stop) {
            shipExhaust.position = CGPointMake(node.position.x + [node childNodeWithName:@"scoutShip"].frame.size.width * .4, node.position.y);
            [self addChild:shipExhaust];
        }];
        
        [self enumerateChildNodesWithName:@"patrolShipRoot" usingBlock:^(SKNode *node, BOOL *stop) {
            SKNode *shipNode = node;
            [node enumerateChildNodesWithName:@"cannon" usingBlock:^(SKNode *node, BOOL *stop) {
                [(Cannon *)node rotateWeapon:node ship:shipNode target:_heroShip screenSize:self.view.frame.size];
            }];
            [(PatrolShip *)node changeState:kStateFiring];
        }];
        
        [self enumerateChildNodesWithName:@"cruiserShipRoot" usingBlock:^(SKNode *node, BOOL *stop) {
            SKNode *shipNode = node;
            [node enumerateChildNodesWithName:@"cannon" usingBlock:^(SKNode *node, BOOL *stop) {
                [(Cannon *)node rotateWeapon:node ship:shipNode target:_heroShip screenSize:self.view.frame.size];
            }];
            [(CrusierShip *)node changeState:kStateFiring];
        }];
        
        [self enumerateChildNodesWithName:@"destroyerShipRoot" usingBlock:^(SKNode *node, BOOL *stop) {
            SKNode *shipNode = node;
            [node enumerateChildNodesWithName:@"cannon" usingBlock:^(SKNode *node, BOOL *stop) {
                [(Cannon *)node rotateWeapon:node ship:shipNode target:_heroShip screenSize:self.view.frame.size];
            }];
            [(Destroyer *)node changeState:kStateFiring];
        }];
        
        [self enumerateChildNodesWithName:@"battleCruiserShipRoot" usingBlock:^(SKNode *node, BOOL *stop) {
            SKNode *shipNode = node;
            shipExhaust.position = CGPointMake(node.position.x + [node childNodeWithName:@"battleCruiserShip"].frame.size.width * .4, node.position.y);
            [self addChild:shipExhaust];
            
            [node enumerateChildNodesWithName:@"cannon" usingBlock:^(SKNode *node, BOOL *stop) {
                [(Cannon *)node rotateWeapon:node ship:shipNode target:_heroShip screenSize:self.view.frame.size];
            }];
            if (_heroShip.shipHit){
                _heroShip.shipHit = NO;
                [(BattleCruiser *)shipNode setWeaponReady:YES];
            }
            [(BattleCruiser *)shipNode changeState:kStateFiring];
            
        }];
        
        if (_heroShip.currentHealth < _heroShip.characterHealth){
            __block int pillCount = 0;
            [self enumerateChildNodesWithName:@"pillNodeRoot" usingBlock:^(SKNode *node, BOOL *stop) {
                if (node.position.x < - node.frame.size.width){
                    [node removeFromParent];
                } else {
                    pillCount ++;
                }
                
            }];
            if (pillCount < 1){
                [self generatePills];
            }
        }
    }
    __block int meteorCount = 0;
    [self enumerateChildNodesWithName:@"meteorNodeRoot" usingBlock:^(SKNode *node, BOOL *stop) {
        if (node.position.x < -node.frame.size.width){
            [node removeFromParent];
        } else {
            meteorCount++;
        }
    }];
    if (meteorCount < 1){
        [self generateMeteors];
    }
    
    
}

-(void)didEvaluateActions{
    
    if (_heroShip.weaponReady == NO){
        _myHUD.weaponsStatusLabel.fontColor = [SKColor redColor];
        _myHUD.weaponsStatusLabel.text = @"WEAPONS CHARGING";
    }
    else {
        _myHUD.weaponsStatusLabel.fontColor = [SKColor greenColor];
        _myHUD.weaponsStatusLabel.text = @"WEAPONS READY";
    }
}

-(SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        if (IS_WIDESCREEN) {
            // iPhone Retina 4-inch
            fileName = [NSString stringWithFormat:@"%@-568", fileName];
        } else {
            // iPhone Retina 3.5-inch
            fileName = fileName;
        }
        
    } else {
        fileName = [NSString stringWithFormat:@"%@-ipad", fileName];
    }
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    
    return textureAtlas;
}

@end
