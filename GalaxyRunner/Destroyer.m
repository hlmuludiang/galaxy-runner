//
//  Destroyer.m
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/3/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "Destroyer.h"

@implementation Destroyer

-(instancetype)initWithSize:(CGSize)size{
    if (self = [super init]){
        self.gameObjectType = kCharacterTypeEnemy;
        self.screenSize = size;
        self.characterHealth = 2.5;
        self.shipSpeed = 7;
        self.currentHealth = self.characterHealth;
        self.cannonSprite1 = [Cannon new];
        self.cannonSprite1.name = @"cannon";
        self.cannonSprite2 = [Cannon new];
        self.cannonSprite2.name = @"cannon";
    }
    return self;
}

-(void)spawnShip{
    _shipSprite = [[SKSpriteNode alloc] initWithTexture:[self.enemyShipAtlas textureNamed:@"destroyer"]];
    _shipSprite.name = @"destroyerShip";
    
    [self addChild:_shipSprite];
    
    self.physicsBody = [SKPhysicsBody bodyWithTexture:[self.enemyShipAtlas textureNamed:@"destroyer"] size:_shipSprite.size];
    self.physicsBody.dynamic = YES;
    self.physicsBody.affectedByGravity = NO;
    self.physicsBody.allowsRotation = NO;
    self.physicsBody.categoryBitMask = enemyShipBitMask;
    self.physicsBody.contactTestBitMask = blueFireBitMask;
    self.physicsBody.collisionBitMask =  0;
    self.physicsBody.usesPreciseCollisionDetection = YES;
    
    [self.cannonSprite1 changeState:kStateSpawning];
    self.cannonSprite1.position = CGPointMake(_shipSprite.frame.size.width * .15, _shipSprite.frame.size.height * .25);
    [self addChild:self.cannonSprite1];
    
    [self.cannonSprite2 changeState:kStateSpawning];
    self.cannonSprite2.position = CGPointMake(_shipSprite.frame.size.width * .15, -_shipSprite.frame.size.height * .25);
    [self addChild:self.cannonSprite2];
    [self healthBar:_shipSprite health:self.characterHealth currentHealth:self.currentHealth];
}


-(void)fireWeapon1{
    if (self.weaponReady){
        self.weaponReady = NO;
        self.fire1 = [FireWeapon new];
        
        [self.fire1 changeState:kStateSpawning];
        self.fire1.position = CGPointMake(-[self.cannonSprite1 childNodeWithName:@"cannon"].frame.size.width / 2, _shipSprite.position.y );
        self.fire1.name = @"blueFire";
        self.fire1.weaponDamage = .2f;
        [self.cannonSprite1 addChild:self.fire1];
        self.fire1.physicsBody.categoryBitMask = enemyFireBitMask;
        SKAction *moveFire = [SKAction moveToX:-self.screenSize.width - _shipSprite.frame.size.width duration: 1];
        SKAction *remove = [SKAction removeFromParent];
        [self.fire1 runAction:[SKAction sequence:@[moveFire, remove]]];
        [self runAction:[SKAction waitForDuration:1] completion:^{
            self.weaponReady = YES;
            [self fireWeapon2];

        }];

    }
}

-(void)fireWeapon2{
    if (self.weaponReady){
        self.weaponReady = NO;
        self.fire2 = [FireWeapon new];
        [self.fire2 changeState:kStateSpawning];
        self.fire2.position = CGPointMake(-[self.cannonSprite2 childNodeWithName:@"cannon"].frame.size.width / 2, _shipSprite.position.y );
        self.fire2.name = @"blueFire";
        self.fire2.weaponDamage = .2f;
        [self.cannonSprite2 addChild:self.fire2];
        self.fire2.physicsBody.categoryBitMask = enemyFireBitMask;
        SKAction *moveFire = [SKAction moveToX:-self.screenSize.width - _shipSprite.frame.size.width duration: 1];
        SKAction *remove = [SKAction removeFromParent];
        [self.fire2 runAction:[SKAction sequence:@[moveFire, remove]]];
        [self runAction:[SKAction waitForDuration:1] completion:^{
            self.weaponReady = YES;
        }];

        
    }
}
-(void)shipDead{
    if (self.currentHealth <= 0){
        self.isAlive = NO;
        [self.cannonSprite removeFromParent];
        [_shipSprite runAction:self.explosionAnimation completion:^{
            [_shipSprite removeFromParent];
        }];
        
    } else {
        [self healthBar:_shipSprite health:self.characterHealth currentHealth:self.currentHealth];
    }
}

-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            [self spawnShip];
            break;
        case kStateFiring:
            [self fireWeapon1];
            break;
        case kStateDead:
            [self shipDead];
        default:
            break;
    }
}

@end
