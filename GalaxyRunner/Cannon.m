//
//  Cannon.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/28/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "Cannon.h"

@implementation Cannon

-(instancetype)init{
    if (self = [super init]) {
        _fire = [FireWeapon new];
    }
    return self;
}

-(void)addCannon{
    _cannon = [[SKSpriteNode alloc] initWithTexture:[self.enemyShipAtlas textureNamed:@"cannon"]];
    _cannon.name = @"cannon";
    [self addChild:_cannon];
}

-(void)fireWeapon{
    if (self.weaponReady){
        [_fire changeState:kStateSpawning];
        _fire.position = CGPointMake(-_cannon.frame.size.width / 2, _cannon.position.y );
        _fire.name = @"blueFire";
        //_fire.zRotation = _cannon.zRotation;
        [self addChild:_fire];
        //NSLog(@"enemy fire %@", [fire childNodeWithName:@"blueFire"].description);
        _fire.physicsBody.categoryBitMask = enemyShipBitMask;
        //SKAction *moveFire = [SKAction moveToX:-self.screenSize.width - _cannon.frame.size.width duration: 1];
        ///SKAction *moveFire = [SKAction moveTo:CGPointMake(0, 0) duration:1];
        SKAction *moveFire = [SKAction moveBy:self.firingAngle duration:1];
        //SKAction *moveFire = [SKAction moveTo:self.targetLocation duration:1];
        //SKAction *moveFire = [SKAction moveByX:self.targetLocation.x y:self.targetLocation.y duration:1];
        SKAction *remove = [SKAction removeFromParent];
        [_fire runAction:[SKAction sequence:@[moveFire, remove]]];
        [_fire.physicsBody applyImpulse:self.firingAngle];
        self.weaponReady = NO;
    }

}

-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            [self addCannon];
            break;
        case kStateFiring:
            [self fireWeapon];
            break;
        default:
            break;
    }
}

@end
