//
//  PatrolShip.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 10/2/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "EnemyShips.h"

@interface PatrolShip : EnemyShips

@property (nonatomic) float characterHealth;
@property (nonatomic) SKSpriteNode *shipSprite;

-(instancetype)initWithSize:(CGSize)size;
-(void)spawnShip;

@end
