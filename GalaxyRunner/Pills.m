//
//  Pills.m
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/8/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "Pills.h"

@implementation Pills

-(instancetype)init{
    if (self = [super init]){
        _pillTextureAtlas = [self textureAtlasNamed:@"pill"];
        _pillStrength = .5;
    }
    return self;
}

-(void)createPill{
    _pillTexturesArray = [NSMutableArray new];
    _pillsRotatingArray = [NSMutableArray new];
    NSArray *textureNames = [[_pillTextureAtlas textureNames] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    for (int temp = 0; temp < textureNames.count; temp++){
        SKTexture *texture = [_pillTextureAtlas textureNamed:textureNames[temp]];
        [_pillTexturesArray addObject:texture];
    }
    _pillNode = [[SKSpriteNode alloc] initWithTexture:[_pillTextureAtlas textureNamed:@"pill01"]];
    
    [self addChild:_pillNode];
    
    self.physicsBody = [SKPhysicsBody bodyWithTexture:[_pillTextureAtlas textureNamed:@"pill01"] size:_pillNode.size];
    self.physicsBody.dynamic = YES;
    self.physicsBody.affectedByGravity = NO;
    self.physicsBody.categoryBitMask = pillBitMask;
    self.physicsBody.allowsRotation = YES;
    self.physicsBody.contactTestBitMask = heroShipBitMask;
    self.physicsBody.collisionBitMask = 0;
    self.physicsBody.usesPreciseCollisionDetection = YES;
    
    SKAction *pillRotatingAction = [SKAction animateWithTextures:_pillTexturesArray timePerFrame:.2 resize:NO restore:NO];
    [_pillNode runAction:[SKAction repeatActionForever:pillRotatingAction]];
}

-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            [self createPill];
            break;
        case kStateDead:
            break;
        default:
            break;
    }
}

@end
