//
//  HighScoresScene.m
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/3/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "HighScoresScene.h"

@implementation HighScoresScene

-(instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]){
    }
    return self;
}

-(void)didMoveToView:(SKView *)view{
    [self showLeaderBoard];
    //[self showGameCenter];
}

-(void)showGameCenter{
    GameManager *sd = [GameManager sharedData];
    GKGameCenterViewController *gc = [[GKGameCenterViewController alloc] init];
    UIViewController *vc = self.view.window.rootViewController;

    GKLocalPlayer *lp = [GKLocalPlayer localPlayer];
    lp.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        [sd setLastError:error];
        
    };
    
    //[sd setAuthenticationViewController:vc];

    
    //[gc presentViewController:sd.authenticationViewController animated:YES completion:nil];
    //[sd authenticateLocalPlayer];
    //[sd setAuthenticationViewController:gc];
    //[[NSNotificationCenter defaultCenter] postNotificationName:PresentAuthenticationViewController object:self];
    
}


-(void)showLeaderBoard{
    GKGameCenterViewController *lb = [[GKGameCenterViewController alloc] init];
    lb.viewState = GKGameCenterViewControllerStateDefault;
    lb.gameCenterDelegate = self;
    UIViewController *vc = self.view.window.rootViewController;
    GameManager *gameData = [GameManager sharedData];
    //[gameData setAuthenticationViewController:vc];*/
    //[vc presentViewController:lb animated:YES completion:nil];
    [lb.topViewController presentViewController: gameData.authenticationViewController animated:YES
                                     completion:nil];
    [gameData authenticateLocalPlayer];

}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController{
    UIViewController *vc = self.view.window.rootViewController;
    [vc dismissViewControllerAnimated:YES completion:nil];
}

@end
