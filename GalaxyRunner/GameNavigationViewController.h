//
//  GameNavigationViewController.h
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/15/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameManager.h"
#import <StartApp/StartApp.h>

@interface GameNavigationViewController : UINavigationController {
    STAStartAppAd *_startAppAd;
}

-(void)showAuthenticationViewController;

@end
