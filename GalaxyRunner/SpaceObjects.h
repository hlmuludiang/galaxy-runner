//
//  SpaceDebris.h
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/6/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameCharacter.h"

@interface SpaceObjects : GameCharacter

@property (nonatomic) SKTextureAtlas *largeMeteors;
@property (nonatomic) SKAction *largeMeteorAnimation;

@property (nonatomic) SKTextureAtlas *mediumMeteors;
@property (nonatomic) SKAction *mediumMeteorAnimation;

@property (nonatomic) SKTextureAtlas *smallMeteors;
@property (nonatomic) SKAction *smallMeteorAnimation;

@property (nonatomic) SKSpriteNode *meteorNode;

@property (nonatomic) NSMutableArray *meteorTextures;
@property (nonatomic) NSMutableArray *meteorAnimation;

-(void)meteorCommons;

@end
