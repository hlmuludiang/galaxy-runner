//
//  GameScene.h
//  GalaxyRunner
//

//  Copyright (c) 2014 Muludiang. All rights reserved.
//
// Images: http://millionthvector.blogspot.com/

#import <SpriteKit/SpriteKit.h>
#import "ScoutShip.h"
#import "PatrolShip.h"
#import "CrusierShip.h"
#import "Destroyer.h"
#import "BattleCruiser.h"
#import "Planets.h"
#import "LargeMeteors.h"
#import "MediumMeteors.h"
#import "SmallMeteors.h"
#import "Pills.h"

#import "HeroShip.h"
#import "HUD.h"
#import "FMMParallaxNode.h"
#import "MenuScene.h"
#import "JSGameMenu.h"

@interface GameScene : SKScene <SKPhysicsContactDelegate, UIAlertViewDelegate> {
    int _score;
    int _level;
    BOOL _gameRunning;
    BOOL _restartGame;
}

@property (nonatomic) HeroShip *heroShip;
@property (nonatomic) ScoutShip *reconShip;
@property (nonatomic) PatrolShip *patrolShip;
@property (nonatomic) CrusierShip *cruiser;
@property (nonatomic) Destroyer *destroyerShip;
@property (nonatomic) BattleCruiser *battleCruiserShip;
@property (nonatomic) HUD *myHUD;
@property (nonatomic) FMMParallaxNode *backGround;
@property (nonatomic) SpaceObjects *spaceDebris;
@property (nonatomic) Planets *planets;
@property (nonatomic) Pills *pills;

@property (nonatomic, strong) JSGameMenu *pauseMenu;

@end
