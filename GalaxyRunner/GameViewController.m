//
//  GameViewController.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameViewController.h"


@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation GameViewController

//-(void)viewDidLoad{
 /*[super viewDidLoad];
 
 // Configure the view.
 SKView * skView = (SKView *)self.view;
 skView.showsFPS = YES;
 skView.showsNodeCount = YES;
 Sprite Kit applies additional optimizations to improve rendering performance
 skView.ignoresSiblingOrder = YES;
 
 // Create and configure the scene.
 GameScene *scene = [GameScene unarchiveFromFile:@"GameScene"];
 scene.scaleMode = SKSceneScaleModeAspectFill;
 
 // Present the scene.
 [skView presentScene:scene];*/
//}

-(void)viewDidLayoutSubviews{
    
    /*NSArray *fontFamilies = [UIFont familyNames];
     
     for (int i = 0; i < [fontFamilies count]; i++)
     {
     NSString *fontFamily = [fontFamilies objectAtIndex:i];
     NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
     NSLog (@"%@: %@", fontFamily, fontNames);
     }*/
    
    [super viewDidLayoutSubviews];
    
    
    
    SKView *skView = (SKView *)self.view;
    if (!skView.scene){
        
        skView.showsPhysics = YES;
        skView.showsDrawCount = YES;
        skView.showsFPS = YES;
        skView.showsNodeCount = YES;
        // Sprite Kit applies additional optimizations to improve rendering performance
        //skView.ignoresSiblingOrder = YES;
        
        // Create and configure the scene.
        SKScene *scene = [MenuScene sceneWithSize:skView.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        // Present the scene.
        [skView presentScene:scene transition:[SKTransition doorsOpenHorizontalWithDuration:2]];
    }
    
}




-(void)dealloc {
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    /*if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
     return UIInterfaceOrientationMaskAllButUpsideDown;
     } else {
     return UIInterfaceOrientationMaskAll;
     }*/
    return UIInterfaceOrientationMaskLandscape;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
