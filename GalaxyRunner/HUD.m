//
//  HUD.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/26/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "HUD.h"

@implementation HUD

-(instancetype)initWithSize:(CGSize)size{
    if (self = [super init]){
        [self setUpDisplay:size];
    }
    return self;
}

-(void)setUpDisplay:(CGSize)size{
    _HUDscoreLabel = [SKLabelNode labelNodeWithFontNamed:@"LCDMono2Normal"];
    _HUDscoreLabel.text = [NSString stringWithFormat:@"Score: %04u", 0];
    _HUDscoreLabel.position = CGPointMake(size.width / 2, size.height - 20);
    _HUDscoreLabel.fontSize = 20;
    _HUDscoreLabel.name = @"HUDscoreLabel";
    [self addChild:_HUDscoreLabel];
    
    SKSpriteNode *healthBar = [[SKSpriteNode alloc] initWithTexture:[SKTexture textureWithImageNamed:@"statusbar"]];
    healthBar.size = CGSizeMake(healthBar.size.width * 1.25, healthBar.size.height);
    healthBar.position = CGPointMake(size.width * .25, 20);
    healthBar.name = @"HUDhealthBar";
    [self addChild:healthBar];
    
    SKSpriteNode *weaponsCoolDownBar = [healthBar copy];
    weaponsCoolDownBar.name = @"weaponsCoolDownBar";
    weaponsCoolDownBar.position = CGPointMake(size.width * .75, 20);
    [self addChild:weaponsCoolDownBar];
    
    _weaponsStatusLabel = [SKLabelNode labelNodeWithFontNamed:@"LCDMono2Normal"];
    _weaponsStatusLabel.text = @"WEAPONS ARMED";
    _weaponsStatusLabel.fontColor = [SKColor greenColor];
    _weaponsStatusLabel.fontSize = 14;
    _weaponsStatusLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _weaponsStatusLabel.name = @"weaponsStatus";
    _weaponsStatusLabel.position = CGPointMake(weaponsCoolDownBar.position.x, weaponsCoolDownBar.position.y);
    [self addChild:_weaponsStatusLabel];
}

-(void)updateScore{
    
}

@end
