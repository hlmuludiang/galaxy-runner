//
//  Planets.m
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/6/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "Planets.h"

@implementation Planets

-(instancetype)initWithSize:(CGSize)size{
    if (self = [super init]){
        _planetAtlas = [self textureAtlasNamed:@"planet"];
        _planetAnimation = [SKAction animateWithTextures:@[[_planetAtlas textureNamed:@"planet1"],[_planetAtlas textureNamed:@"planet2"],[_planetAtlas textureNamed:@"planet3"],[_planetAtlas textureNamed:@"planet4"],[_planetAtlas textureNamed:@"planet5"],[_planetAtlas textureNamed:@"planet6"],[_planetAtlas textureNamed:@"planet7"],[_planetAtlas textureNamed:@"planet8"],[_planetAtlas textureNamed:@"planet9"],[_planetAtlas textureNamed:@"planet10"],[_planetAtlas textureNamed:@"planet11"],[_planetAtlas textureNamed:@"planet12"],[_planetAtlas textureNamed:@"planet13"],[_planetAtlas textureNamed:@"planet14"],[_planetAtlas textureNamed:@"planet15"],[_planetAtlas textureNamed:@"planet16"],[_planetAtlas textureNamed:@"planet17"],[_planetAtlas textureNamed:@"planet18"],[_planetAtlas textureNamed:@"planet19"]] timePerFrame:.25 resize:NO restore:YES];
    }
    return self;
}

-(void)createPlanet{
    _planetNode = [[SKSpriteNode alloc] initWithTexture:[_planetAtlas textureNamed:@"planet1"]];
    [self addChild:_planetNode];
    [_planetNode runAction:[SKAction repeatActionForever:_planetAnimation]];

    
}


-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            [self createPlanet];
            break;
        case kStateDead:
            break;
        default:
            break;
    }
}


@end
