//
//  GameViewController.h
//  GalaxyRunner
//

//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "GameNavigationViewController.h"
#import "MenuScene.h"
#import "GameManager.h"

@interface GameViewController : UIViewController

@end
