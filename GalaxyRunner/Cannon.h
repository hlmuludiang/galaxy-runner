//
//  Cannon.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/28/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameCharacter.h"
#import "FireWeapon.h"

@interface Cannon : GameCharacter

@property (nonatomic) SKSpriteNode *cannon;
@property (nonatomic) FireWeapon *fire;

@end
