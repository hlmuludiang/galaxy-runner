//
//  HeroShip.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/24/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameCharacter.h"
#import "FireWeapon.h"

@interface HeroShip : GameCharacter{
    int projectileSpeed;
}


@property (nonatomic) SKSpriteNode *heroShip;

-(instancetype)initWithhSize:(CGSize)size;
@property (nonatomic) SKTextureAtlas *heroShipAtlas;

@property (nonatomic)    SKShapeNode *healthBarOUtline;

@property (nonatomic) BOOL shipHit;

@end
