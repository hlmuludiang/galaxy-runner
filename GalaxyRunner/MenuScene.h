//
//  MenuScene.h
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/3/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameScene.h"
#import "SettingScene.h"
#import "HighScoresScene.h"
#import "HelpScene.h"
#import <StartApp/StartApp.h>

@interface MenuScene : SKScene {
    STAStartAppAd *_showAd;
}

@end
