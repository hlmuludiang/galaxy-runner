//
//  Planets.h
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/6/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "SpaceObjects.h"

@interface Planets : SpaceObjects

@property (nonatomic) SKTextureAtlas *planetAtlas;
@property (nonatomic) SKAction *planetAnimation;
@property (nonatomic) SKSpriteNode *planetNode;

-(instancetype)initWithSize:(CGSize)size;

@end
