//
//  LargeMeteors.m
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/7/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "LargeMeteors.h"

@implementation LargeMeteors

-(instancetype)init{
    if (self = [super init]){
        
    }
    return self;
}

-(void)createLargeMeteor{
    self.meteorTextures = [NSMutableArray new];
    self.meteorAnimation = [NSMutableArray new];
    NSArray *textureNames = [[self.largeMeteors textureNames] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    for (int temp = 0; temp < textureNames.count; temp++){
        SKTexture *texture = [self.largeMeteors textureNamed:textureNames[temp]];
        [self.meteorTextures addObject:texture];
    }
    int y = self.meteorTextures.count / 16;
    int z = arc4random() % y;
    self.meteorNode = [[SKSpriteNode alloc] initWithTexture:self.meteorTextures[(16 * z)]];
    self.meteorNode.physicsBody.categoryBitMask = largeMeteorBitMask;
    
    self.meteorNode.physicsBody = [SKPhysicsBody bodyWithTexture:self.meteorTextures[16 *z] size:self.meteorNode.size];
    
    for (int a = (16 * z); a <= (16 * z) + 15; a++){
        [self.meteorAnimation addObject:self.meteorTextures[a]];
    }
    [self meteorCommons];
}

-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            [self createLargeMeteor];
            break;
        case kStateDead:
            break;
        default:
            break;
    }
}

@end
