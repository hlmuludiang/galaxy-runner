///
//  main.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
