//
//  CommonProtocols.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/24/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#ifndef GalaxyRunner_CommonProtocols_h
#define GalaxyRunner_CommonProtocols_h

typedef enum {
    kCharacterTypeNone,
    kCharacterTypeHero,
    kCharacterTypeEnemy,
    kCharacterTypePowerUp,
    kCharacterTypeCoin
} GameObjectType;

typedef enum {
    kStateSpawning,
    kStateIdle,
    kStateMoving,
    kStateWalking,
    KStateJumping,
    kStateLanding,
    kStatePickedUp,
    kStateDead,
    kStateFiring
} CharacterStates;

@interface SKEmitterNode (fromFile)
+ (instancetype)emitterNamed:(NSString*)name;
@end

@implementation SKEmitterNode (fromFile)
+ (instancetype)emitterNamed:(NSString*)name
{
    return [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle] pathForResource:name ofType:@"sks"]];
}
@end

#endif