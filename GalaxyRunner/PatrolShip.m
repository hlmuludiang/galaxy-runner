//
//  PatrolShip.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 10/2/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "PatrolShip.h"

@implementation PatrolShip

-(instancetype)initWithSize:(CGSize)size{
    if (self = [super init]){
        self.gameObjectType = kCharacterTypeEnemy;
        self.screenSize = size;
        self.characterHealth = .5;
        self.shipSpeed = 3;
        self.currentHealth = self.characterHealth;
        self.fire = [FireWeapon new];
        self.cannonSprite = [Cannon new];
        
    }
    return self;
}

-(void)spawnShip{
    _shipSprite = [[SKSpriteNode alloc] initWithTexture:[self.enemyShipAtlas textureNamed:@"patrol"]];
    _shipSprite.name = @"patrolShip";
    
    [self addChild:_shipSprite];
    
    self.physicsBody = [SKPhysicsBody bodyWithTexture:[self.enemyShipAtlas textureNamed:@"patrol"] size:_shipSprite.size];
    self.physicsBody.dynamic = YES;
    self.physicsBody.affectedByGravity = NO;
    self.physicsBody.allowsRotation = NO;
    self.physicsBody.categoryBitMask = enemyShipBitMask;
    self.physicsBody.contactTestBitMask = blueFireBitMask;
    self.physicsBody.collisionBitMask =  0;
    self.physicsBody.usesPreciseCollisionDetection = YES;
    
    [self healthBar:_shipSprite health:self.characterHealth currentHealth:self.currentHealth];
}


-(void)fireWeapon{
    if (self.weaponReady){
        self.weaponReady = NO;
        [self.fire changeState:kStateSpawning];
        self.fire.weaponDamage = .1;
        self.fire.position = CGPointMake(-_shipSprite.frame.size.width / 2, _shipSprite.position.y );
        self.fire.name = @"blueFire";
        [_shipSprite addChild:self.fire];
        
        self.fire.physicsBody.categoryBitMask = enemyFireBitMask;
        SKAction *moveFire = [SKAction moveToX:-self.screenSize.width - _shipSprite.frame.size.width duration: 1];
        SKAction *remove = [SKAction removeFromParent];
        [self.fire runAction:[SKAction sequence:@[moveFire, remove]] completion:^{
            self.weaponReady = YES;
        }];
    }
}

-(void)shipDead{
    if (self.currentHealth <= 0){
        self.isAlive = NO;
        [_shipSprite runAction:self.explosionAnimation completion:^{
            [_shipSprite removeFromParent];
        }];
        
    } else {
        [self healthBar:_shipSprite health:self.characterHealth currentHealth:self.currentHealth];
    }
}

-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            [self spawnShip];
            break;
        case kStateFiring:
            [self fireWeapon];
            break;
        case kStateDead:
            [self shipDead];
        default:
            break;
    }
}

@end
