//
//  GameObject.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameObject.h"

@implementation GameObject

-(instancetype)init{
    if (self = [super init]){
        _gameObjectType = kCharacterTypeNone;
        _projectilesAtlas = [self textureAtlasNamed:@"projectiles"];
        _backgroundAtlas = [self textureAtlasNamed:@"background"];
        _explosionAtlas = [self textureAtlasNamed:@"explosion"];
        _explosionAnimation = [SKAction animateWithTextures:@[[_explosionAtlas textureNamed:@"explosion1"],[_explosionAtlas textureNamed:@"explosion2"],[_explosionAtlas textureNamed:@"explosion3"],[_explosionAtlas textureNamed:@"explosion4"],[_explosionAtlas textureNamed:@"explosion4"],[_explosionAtlas textureNamed:@"explosion5"],[_explosionAtlas textureNamed:@"explosion6"],[_explosionAtlas textureNamed:@"explosion7"]] timePerFrame:.1 resize:NO restore:NO];
    }
    return self;
}

-(void)changeState:(CharacterStates)newState{
    NSLog(@"GameObject -> changeState need to be overridden");
}

-(SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        if (IS_WIDESCREEN) {
            // iPhone Retina 4-inch
            fileName = [NSString stringWithFormat:@"%@-568", fileName];
        } else {
            // iPhone Retina 3.5-inch
            fileName = fileName;
        }
        
    } else {
        fileName = [NSString stringWithFormat:@"%@-ipad", fileName];
    }
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    
    return textureAtlas;
}


@end
