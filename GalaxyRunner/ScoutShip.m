//
//  BaseShip.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "ScoutShip.h"

@implementation ScoutShip

-(instancetype)initWithSize:(CGSize)size{
    if (self = [super init]){
        self.gameObjectType = kCharacterTypeEnemy;
        self.screenSize = size;
        self.characterHealth = .5;
        self.shipSpeed = 2;
        self.currentHealth = self.characterHealth;
    }
    return self;
}

-(void)spawnShip{
    _shipSprite = [[SKSpriteNode alloc] initWithTexture:[self.enemyShipAtlas textureNamed:@"scout"]];
    _shipSprite.name = @"scoutShip";

    [self addChild:_shipSprite];
    
    self.physicsBody = [SKPhysicsBody bodyWithTexture:[self.enemyShipAtlas textureNamed:@"scout"] size:_shipSprite.size];
    self.physicsBody.dynamic = YES;
    self.physicsBody.affectedByGravity = NO;
    self.physicsBody.allowsRotation = NO;
    self.physicsBody.categoryBitMask = enemyShipBitMask;
    self.physicsBody.contactTestBitMask = blueFireBitMask;
    self.physicsBody.collisionBitMask =  0;
    self.physicsBody.usesPreciseCollisionDetection = YES;
    


    [self healthBar:_shipSprite health:self.characterHealth currentHealth:self.currentHealth];
}

-(void)shipMoving{
    NSString *emitterName = [[NSBundle mainBundle] pathForResource:@"ShipExhaust" ofType:@"sks"];
    SKEmitterNode *shipExhaust = [NSKeyedUnarchiver unarchiveObjectWithFile:emitterName];
    shipExhaust.position = CGPointMake(_shipSprite.frame.size.width / 2, 0);
    [self addChild:shipExhaust];
}

-(void)shipDead{
    if (self.currentHealth <= 0){
        self.isAlive = NO;
        //[_cannonSprite removeFromParent];
         [_shipSprite runAction:self.explosionAnimation completion:^{
            [_shipSprite removeFromParent];
        }];

    } else {
        [self healthBar:_shipSprite health:self.characterHealth currentHealth:self.currentHealth];
    }
}

-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            [self spawnShip];
            break;
        case kStateFiring:
            //[self fireWeapon];
            break;
        case kStateDead:
            [self shipDead];
        case kStateMoving:
            [self shipMoving];
            break;
        default:
            break;
    }
}



@end
