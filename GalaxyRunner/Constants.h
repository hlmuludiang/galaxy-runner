//
//  Constants.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <stdint.h>

#ifndef GalaxyRunner_Constants_h
#define GalaxyRunner_Constants_h

// From: http://www.raywenderlich.com/57368/trigonometry-game-programming-sprite-kit-version-part-1
#define SK_DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) * 0.01745329252f) // PI / 180
#define SK_RADIANS_TO_DEGREES(__ANGLE__) ((__ANGLE__) * 57.29577951f) // PI * 180
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

static const uint32_t heroShipBitMask = 0x1 << 0;
static const uint32_t blueFireBitMask = 0x1 << 1;
static const uint32_t enemyShipBitMask = 0x1 << 2;
static const uint32_t worldBitMask = 0x1 << 3;
static const uint32_t pillBitMask = 0x1 << 4;
static const uint32_t largeMeteorBitMask = 0x1 << 5;
static const uint32_t mediumMeteorBitMask = 0x1 << 6;
static const uint32_t smallMeteorBitMask = 0x1 << 7;
static const uint32_t enemyFireBitMask = 0x1 << 8;

#endif
