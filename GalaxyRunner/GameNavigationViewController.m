//
//  GameNavigationViewController.m
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/15/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameNavigationViewController.h"

@interface GameNavigationViewController ()

@end

@implementation GameNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _startAppAd = [[STAStartAppAd alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    [_startAppAd loadAd];
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAuthenticationViewController) name:PresentAuthenticationViewController object:nil];
    [[GameManager sharedData] authenticateLocalPlayer];
}


-(void)showAuthenticationViewController{
    GameManager *gameData = [GameManager sharedData];
    
    [self.topViewController presentViewController:gameData.authenticationViewController
                                           animated:YES
                                         completion:nil];
}

-(void)dealloc{
    NSLog(@"Removing listener");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
