//
//  HighScoresScene.h
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/3/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameManager.h"

@interface HighScoresScene : SKScene <GKGameCenterControllerDelegate>

@end
