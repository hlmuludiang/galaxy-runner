//
//  Destroyer.h
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/3/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "EnemyShips.h"

@interface Destroyer : EnemyShips

@property (nonatomic) float characterHealth;
@property (nonatomic) SKSpriteNode *shipSprite;

-(instancetype)initWithSize:(CGSize)size;
-(void)spawnShip;

@end
