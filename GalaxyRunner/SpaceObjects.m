//
//  SpaceDebris.m
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/6/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "SpaceObjects.h"

@implementation SpaceObjects

-(instancetype)init{
    if (self = [super init]){
        _largeMeteors = [self textureAtlasNamed:@"meteor-large"];
        _mediumMeteors = [self textureAtlasNamed:@"meteor-medium"];
        _smallMeteors = [self textureAtlasNamed:@"meteor-small"];
        //self.screenSize = size;
    }
    return self;
}


-(void)meteorCommons{
    _meteorNode.position = CGPointMake([self getRandomNumberBetween:0 to:self.screenSize.width / 2],[self getRandomNumberBetween:-self.screenSize.height / 2 to:self.screenSize.height / 2]);
    _meteorNode.name = @"meteor";
    [self addChild:_meteorNode];
    _meteorNode.physicsBody.affectedByGravity = NO;
    _meteorNode.physicsBody.dynamic = YES;
    _meteorNode.physicsBody.allowsRotation = NO;
    _meteorNode.physicsBody.contactTestBitMask = blueFireBitMask;
    _meteorNode.physicsBody.collisionBitMask =  largeMeteorBitMask | mediumMeteorBitMask | smallMeteorBitMask;
    _meteorNode.physicsBody.usesPreciseCollisionDetection = YES;
    
    SKAction *rotateMeteor = [SKAction animateWithTextures:_meteorAnimation timePerFrame:.2];
    //SKAction *moveMeteor = [SKAction moveToX:-_spaceDebris.frame.size.width   duration:7];
    //SKAction *removeMeteor = [SKAction removeFromParent];
    [_meteorNode runAction:[SKAction repeatActionForever:rotateMeteor]];
    //[_meteorNode runAction:[SKAction sequence:@[moveMeteor, removeMeteor]]];
}





-(void)changeState:(CharacterStates)newState{
    [self setCharacterState:newState];
    switch (newState) {
        case kStateSpawning:
            break;
        case kStateDead:
            break;
        default:
            break;
    }
}
@end
