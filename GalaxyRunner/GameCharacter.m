//
//  GameCharacter.m
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameCharacter.h"

@implementation GameCharacter

-(id)init {
    if (self = [super init]) {
        _characterHealth = 0;
        _roundAlive = YES;
        _isAlive = YES;
        _weaponReady = YES;
                _enemyShipAtlas = [self textureAtlasNamed:@"enemies"];

    }
    return self;
}

-(void)healthBar:(SKNode *)node health:(float)shipHealth currentHealth:(float)health{
    [node removeAllChildren];
    float HealthBarWidth = node.frame.size.width * health/shipHealth;
    float HealthBarHeight = 4.0f;
    SKShapeNode *healthBarOUtline = [SKShapeNode shapeNodeWithRect:CGRectMake(0, 0, HealthBarWidth, HealthBarHeight)];
    healthBarOUtline.strokeColor = [SKColor brownColor];
    healthBarOUtline.lineWidth = 2.0f;
    healthBarOUtline.fillColor = [SKColor whiteColor];
    healthBarOUtline.position = CGPointMake( -node.frame.size.width / 2, -node.frame.size.height / 2 - healthBarOUtline.frame.size.height );
    [node addChild:healthBarOUtline];
}

-(void)rotateWeapon:(SKNode *)cannon ship:(SKNode *)ship target:(SKNode *)target screenSize:(CGSize)screen{
        float targetX = target.position.x;
        float targetY = target.position.y;
        float offX = targetX - ship.position.x - [cannon childNodeWithName:@"cannon"].frame.size.width / 2;
        float offY = targetY - ship.position.y;
        float targetAngle = atan2f(offY * 2, offX * 2);
        cannon.zRotation = targetAngle - SK_DEGREES_TO_RADIANS(180);
        _firingAngle = CGVectorMake(offX * 2, offY * 2);

}

-(void)shipPath:(SKNode *)node frameSize:(CGSize)size shipSpeed:(int)speed{
    CGMutablePathRef cgpath = CGPathCreateMutable();
    
    //random values
    float xStart = [self getRandomNumberBetween:0+node.frame.size.height to:size.height-node.frame.size.height ];
    float xEnd = [self getRandomNumberBetween:0+node.frame.size.height to:size.height-node.frame.size.height ];
    
    //ControlPoint1
    float cp1X = [self getRandomNumberBetween:0+node.frame.size.height to:size.height-node.frame.size.height ];
    float cp1Y = [self getRandomNumberBetween:0+node.frame.size.height to:size.height-node.frame.size.width ];
    
    //ControlPoint2
    float cp2X = [self getRandomNumberBetween:0+node.frame.size.height to:size.height-node.frame.size.height ];
    float cp2Y = [self getRandomNumberBetween:0 to:cp1Y];
    
    CGPoint s = CGPointMake(size.width + node.frame.size.width , xStart);
    CGPoint e = CGPointMake(-100, xEnd);
    CGPoint cp1 = CGPointMake(cp1X, cp1Y);
    CGPoint cp2 = CGPointMake(cp2X, cp2Y);
    CGPathMoveToPoint(cgpath,NULL, s.x, s.y);
    CGPathAddCurveToPoint(cgpath, NULL, cp1.x, cp1.y, cp2.x, cp2.y, e.x, e.y);
    SKAction *planeDestroy = [SKAction followPath:cgpath asOffset:YES orientToPath:NO duration:speed];
    SKAction *remove = [SKAction removeFromParent];
    [node runAction:[SKAction sequence:@[planeDestroy, remove]]];
    
    CGPathRelease(cgpath);
}



-(int)getRandomNumberBetween:(int)from to:(int)to {
    
    //return (int)from + arc4random() % (to-from+1);
    return (int)from + arc4random_uniform(to - from + 1);
}

@end
