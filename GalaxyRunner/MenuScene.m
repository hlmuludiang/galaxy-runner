//
//  MenuScene.m
//  GalaxyRunner
//
//  Created by Versatile Systems, Inc on 10/3/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "MenuScene.h"


@implementation MenuScene

-(instancetype)initWithSize:(CGSize)size{
    if (self = [super initWithSize:size]){
        [self createSceneContents];
    }
    return self;
}

-(void)didMoveToView:(SKView *)view{
    [_showAd showAd];
}

-(void)createSceneContents{
    SKLabelNode *playGame = [[SKLabelNode alloc] initWithFontNamed:@"DystopianFuture"];
    playGame.text = @"Play Game";
    playGame.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 + playGame.frame.size.height * 2);
    playGame.name = @"playGameButton";
    [self addChild:playGame];
    
    SKLabelNode *highScores = [[SKLabelNode alloc] initWithFontNamed:@"DystopianFuture"];
    highScores.text = @"High Scores";
    highScores.position = CGPointMake(playGame.position.x, playGame.position.y - (highScores.frame.size.height * 1.5));
    highScores.name = @"highScoresButton";
    [self addChild:highScores];
    
    SKLabelNode *settingsButton = [[SKLabelNode alloc] initWithFontNamed:@"DystopianFuture"];
    settingsButton.text = @"Game Settings";
    settingsButton.position = CGPointMake(highScores.position.x, highScores.position.y - (settingsButton.frame.size.height * 1.5));
    settingsButton.name = @"gameSettingsButton";
    [self addChild:settingsButton];
    
    SKLabelNode *helpButton = [[SKLabelNode alloc] initWithFontNamed:@"DystopianFuture"];
    helpButton.text = @"Help";
    helpButton.position = CGPointMake(settingsButton.position.x, settingsButton.position.y - (helpButton.frame.size.height * 1.5));
    helpButton.name = @"helpButton";
    [self addChild:helpButton];
    
    SKLabelNode *creditsButton = [[SKLabelNode alloc] initWithFontNamed:@"DystopianFuture"];
    creditsButton.text = @"Credits";
    creditsButton.position = CGPointMake(helpButton.position.x, helpButton.position.y - (creditsButton.frame.size.height * 1.5));
    creditsButton.name = @"creditsButton";
    [self addChild:creditsButton];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    for (UITouch *touch in touches){
        CGPoint touchPosition = [touch locationInNode:self];
        if ([[self nodeAtPoint:touchPosition].name isEqualToString:@"playGameButton"]){
            // Create and configure the scene.
            SKScene *scene = [GameScene sceneWithSize:self.frame.size];
            scene.scaleMode = SKSceneScaleModeAspectFill;
            // Present the scene.
            [self.view presentScene:scene];
        } else if ([[self nodeAtPoint:touchPosition].name isEqualToString:@"highScoresButton"]){
            if ([GKLocalPlayer localPlayer].authenticated){
            SKScene *scene = [HighScoresScene sceneWithSize:self.frame.size];
            scene.scaleMode = SKSceneScaleModeAspectFill;
                [self.view presentScene:scene];
            } else {
                UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Oops..." message:@"You need to log into Game Center to view the leaderboard." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                [alertView show];}
        } else if ([[self nodeAtPoint:touchPosition].name isEqualToString:@"settingsButton"]){
            
        } else if ([[self nodeAtPoint:touchPosition].name isEqualToString:@"helpButton"]){
            
        } else if ([[self nodeAtPoint:touchPosition].name isEqualToString:@"creditsButton"]){
            
        }
    }
}

@end
