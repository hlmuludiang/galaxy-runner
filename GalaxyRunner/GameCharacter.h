//
//  GameCharacter.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 9/23/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameObject.h"

@interface GameCharacter : GameObject

@property (nonatomic) float characterHealth;
@property (nonatomic) float currentHealth;
@property (nonatomic) CharacterStates characterState;
@property (nonatomic) CGSize screenSize;
@property (nonatomic) BOOL roundAlive;
@property (nonatomic) BOOL isAlive;
@property (nonatomic) BOOL weaponReady;
@property (nonatomic) CGVector firingAngle;
@property (nonatomic) CGPoint targetLocation;
@property (nonatomic) SKTextureAtlas *enemyShipAtlas;

//-(void)healthBar:(SKNode *)node;
-(void)healthBar:(SKNode *)node health:(float)shipHealth currentHealth:(float)health;
-(void)rotateWeapon:(SKNode *)node ship:(SKNode *)ship target:(SKNode *)target screenSize:(CGSize)screen;
-(void)shipPath:(SKNode *)node frameSize:(CGSize)size shipSpeed:(int)speed;
-(int)getRandomNumberBetween:(int)from to:(int)to;

@end
