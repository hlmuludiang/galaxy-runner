//
//  EnemyShips.h
//  GalaxyRunner
//
//  Created by Hilary Muludiang on 10/2/14.
//  Copyright (c) 2014 Muludiang. All rights reserved.
//

#import "GameCharacter.h"
#import "Cannon.h"
#import "FireWeapon.h"
#import "HeroShip.h"

@interface EnemyShips : GameCharacter

@property (nonatomic) FireWeapon *fire;
@property (nonatomic) FireWeapon *fire1;
@property (nonatomic) FireWeapon *fire2;
@property (nonatomic) Cannon *cannonSprite;
@property (nonatomic) Cannon *cannonSprite1;
@property (nonatomic) Cannon *cannonSprite2;
@property (nonatomic) int shipSpeed;

@end
